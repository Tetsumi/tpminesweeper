"
A clone of minesweeper.
"
Class {
	#name : #TMinesweeper,
	#superclass : #Model,
	#instVars : [
		'window',
		'board'
	],
	#category : #Tpminesweeper
}

{ #category : #initialization }
TMinesweeper >> initialize [
	board := TMinesweeperBoard new.
	
   "Setup window"
	window := StandardWindow new.
	window model: self.
	window title: 'Minesweeper'.
	
	"New game"
	window addMorph: (window newButtonFor: board
				getState: nil
				action: #newGame
				arguments: nil
				getEnabled: nil
				label: 'New game'
				help: 'Start a new game.')
				fullFrame: 
				(LayoutFrame  fractions: (0@0   corner: 0.25@0) offsets: (0@0 corner: 0@50)).
				
	"Beginner"
	window addMorph: (window newButtonFor: board
				getState: nil
				action: #setDifficulty:
				arguments: #(#beginner)
				getEnabled: nil
				label: 'Beginner'
				help: 'Set difficulty to beginner level.')
				fullFrame: 
				(LayoutFrame  fractions: (0.25@0   corner: 0.50@0) offsets: (0@0 corner: 0@50)).
				
	"Intermediate"
	window addMorph: (window newButtonFor: board
				getState: nil
				action: #setDifficulty:
				arguments: #(#intermediate)
				getEnabled: nil
				label: 'Intermediate'
				help: 'Set difficulty to intermediate level.')
				fullFrame: 
				(LayoutFrame  fractions: (0.50@0   corner: 0.75@0) offsets: (0@0 corner: 0@50)).
				
	"Expert"
	window addMorph: (window newButtonFor: board
				getState: nil
				action: #setDifficulty:
				arguments: #(#expert)
				getEnabled: nil
				label: 'Expert'
				help: 'Set difficulty to expert level.')
				fullFrame: 
				(LayoutFrame  fractions: (0.75@0   corner: 1@0) offsets: (0@0 corner: 0@50)).
		  	
	window addMorph: board
	  fullFrame: ((0 @ 0 corner: 1 @ 1) asLayoutFrame topOffset: 55).
	
	window openInWorld.
]
