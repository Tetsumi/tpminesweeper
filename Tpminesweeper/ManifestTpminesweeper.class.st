"
I store metadata for this package. These meta data are used by other tools such as the SmalllintManifestChecker and the critics Browser
"
Class {
	#name : #ManifestTpminesweeper,
	#superclass : #PackageManifest,
	#category : #Tpminesweeper
}

{ #category : #'meta-data' }
ManifestTpminesweeper class >> description [ ^ 'A minesweeper game.'
]
