"
Implement the cells matrix, difficulty, gameplay logic.
"
Class {
	#name : #TMinesweeperBoard,
	#superclass : #BorderedMorph,
	#instVars : [
		'cells',
		'flags',
		'difficulty',
		'catalog',
		'matrix',
		'cellsOpen'
	],
	#classVars : [
		'CatalogWeak'
	],
	#category : #Tpminesweeper
}

{ #category : #accessing }
TMinesweeperBoard class >> catalog [
	CatalogWeak 
		ifNil: 
			[ CatalogWeak := WeakArray new: 1.
				^self loadCatalog. ]
		ifNotNil: 
			[ | cat |
				cat := CatalogWeak at:1.
				(cat) 
					ifNil: [ ^self loadCatalog. ]
					ifNotNil: [ ^cat. ].]. 
]

{ #category : #private }
TMinesweeperBoard class >> loadCatalog [
	| cat imgForm |
	imgForm := Form fromFileNamed: 'tminesweeper/minesweeper.png'.
	cat := Array new: 16.
	
	cat
		"open 0 - 9" 
		at: 1 put: (imgForm contentsOfArea: (Rectangle origin: 19 @ 53 extent: 16 @ 16));
	 	at: 2 put: (imgForm contentsOfArea: (Rectangle origin: 2 @ 70 extent: 16 @ 16));
		at: 3 put: (imgForm contentsOfArea: (Rectangle origin: 19 @ 70 extent: 16 @ 16));
		at: 4 put: (imgForm contentsOfArea: (Rectangle origin: 36 @ 70 extent: 16 @ 16));
		at: 5 put: (imgForm contentsOfArea: (Rectangle origin: 53 @ 70 extent: 16 @ 16));
		at: 6 put: (imgForm contentsOfArea: (Rectangle origin: 70 @ 70 extent: 16 @ 16));
		at: 7 put: (imgForm contentsOfArea: (Rectangle origin: 87 @ 70 extent: 16 @ 16));
		at: 8 put: (imgForm contentsOfArea: (Rectangle origin: 104 @ 70 extent: 16 @ 16));
		at: 9 put: (imgForm contentsOfArea: (Rectangle origin: 121 @ 70 extent: 16 @ 16));
		"closed"
		at: 10 put: (imgForm contentsOfArea: (Rectangle origin: 2 @ 53 extent: 16 @ 16));
		"flagged"
		at: 11 put: (imgForm contentsOfArea: (Rectangle origin: 36 @ 53 extent: 16 @ 16));
		"open bomb"
		at: 12 put: (imgForm contentsOfArea: (Rectangle origin: 87 @ 53 extent: 16 @ 16));
		"open bomb red"
		at: 13 put: (imgForm contentsOfArea: (Rectangle origin: 104 @ 53 extent: 16 @ 16));
		"open bomb crossed"
		at: 14 put: (imgForm contentsOfArea: (Rectangle origin: 121 @ 53 extent: 16 @ 16));
		"closed and when mouse over"
		at: 15 put: (imgForm contentsOfArea: (Rectangle origin: 2 @ 94 extent: 16 @ 16)).
		
	CatalogWeak at: 1 put: cat.
	^cat.
]

{ #category : #'as yet unclassified' }
TMinesweeperBoard >> difficultyBombs [
	difficulty = #beginner
		ifTrue: [ ^ 10 ].
	difficulty = #intermediate
		ifTrue: [ ^ 40 ].
	^ 99
]

{ #category : #'as yet unclassified' }
TMinesweeperBoard >> difficultyColumns [
	(difficulty = #beginner)
		ifTrue: [ ^ 8 ].
		
	(difficulty = #intermediate)
		ifTrue: [ ^ 16 ].
		
	^ 30.
]

{ #category : #'as yet unclassified' }
TMinesweeperBoard >> difficultyRows [
	(difficulty = #beginner)
		ifTrue: [ ^ 8 ].
	^ 16
]

{ #category : #'as yet unclassified' }
TMinesweeperBoard >> endGame [
	cells
		do: [ :cell |
			cell lock.  
			cell isBomb 
				ifTrue: [ cell isOpen ifFalse: [ cell form: (catalog at: 12) ] ]
				ifFalse: [ cell isFlagged ifTrue: [ cell form: (catalog at: 14) ]]]
]

{ #category : #accessing }
TMinesweeperBoard >> flags [
	^flags.
]

{ #category : #'as yet unclassified' }
TMinesweeperBoard >> gameOver [
	self endGame
]

{ #category : #initialization }
TMinesweeperBoard >> initialize [
	super initialize.
	flags := 0.
	self setDifficulty: #beginner.
	catalog := TMinesweeperBoard catalog.
	cellsOpen := 0.
	self color: Color transparent.
	self newGame.
	
	
	
]

{ #category : #adding }
TMinesweeperBoard >> neighboursAt: x At:y Do: aBlock [
	| maxX maxY |
	maxX := matrix numberOfColumns.
	maxY := matrix numberOfRows.
	
	"TOP ROW"
	(y > 1) ifTrue: [ 
		aBlock value: (matrix at: y - 1 at: x).
		(x > 1) ifTrue: [  aBlock value: (matrix at: y - 1 at: x  - 1) ].
		(x < maxX) ifTrue: [ aBlock value: (matrix at: y - 1 at: x + 1) ]].
	"MIDDLE ROW"
	(x > 1) ifTrue: [  aBlock value: (matrix at: y at: x - 1) ].
	(x < maxX) ifTrue: [ aBlock value: (matrix at: y at: x + 1) ].
	"BOTTOM ROW"
	(y < maxY) ifTrue:[
		aBlock value: (matrix at: y + 1 at: x).
		(x > 1) ifTrue: [  aBlock value: (matrix at: y + 1 at: x - 1) ].
		(x < maxX) ifTrue: [ aBlock value: (matrix at: y + 1 at: x + 1) ]]
]

{ #category : #'instance creation' }
TMinesweeperBoard >> newGame [
  | origin |
	cells do: [ :cell | cell reset].
	cells shuffle.
	origin := self innerBounds origin.
	matrix withIndicesDo:  [ :c :y :x |
		c unlock.
		c position: ((x - 1) * c width) @ ((y - 1) * c height) + origin.
		c setPositionAt: x at: y.
		(c isBomb) ifTrue: [ self neighboursAt: x At: y Do: [ :cell | cell bombsCounterAdd: 1 ] ].
		"c lock."]
]

{ #category : #'as yet unclassified' }
TMinesweeperBoard >> onCellOpen [
	cellsOpen := cellsOpen + 1.
	(cellsOpen = ((cells size) - self difficultyBombs))
		ifTrue: [ self endGame ]
]

{ #category : #accessing }
TMinesweeperBoard >> removeFlagAt: x at: y [
	flags := flags - 1.
]

{ #category : #initialization }
TMinesweeperBoard >> setDifficulty: newDifficulty [
	| aBombs rows columns|
	
	(newDifficulty = difficulty)
		ifTrue: [ ^ self ].
	"REMOVE OLD CELL MORPHS IF PRESENT"
	cells ifNotNil: [cells do: [ :cell | self removeMorph: cell ]].
	difficulty := newDifficulty.
	rows := self difficultyRows.
	columns := self difficultyColumns.	
	cells := Array new: rows * columns.
	aBombs := self difficultyBombs.
	self extent: (16 * columns)@(16 * rows) + (2 * self borderWidth).
	1 to: cells size do: [ :i || cell | 
		cell := ((i > aBombs) ifTrue: [TMinesweeperCellEmpty] ifFalse: [TMinesweeperCellBomb]) new.
		cells at: i put: cell.
		self addMorph: cell].
	
	matrix := Matrix rows: rows columns: columns contents: cells.
	
	self newGame.
]
