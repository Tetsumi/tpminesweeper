Class {
	#name : #TMinesweeperCellBomb,
	#superclass : #TMinesweeperCell,
	#category : #Tpminesweeper
}

{ #category : #testing }
TMinesweeperCellBomb >> isBomb [
	^true.
]

{ #category : #testing }
TMinesweeperCellBomb >> onOpen [
	self form: (TMinesweeperBoard catalog at: 13).
	self owner gameOver.
	
]
