"
A flagged cell can't be visited.
Visiting a bomb ends the game.

status:
	-  #close
	-  #open
	-  #flagged
	
"
Class {
	#name : #TMinesweeperCell,
	#superclass : #ImageMorph,
	#instVars : [
		'status',
		'x',
		'y',
		'bombsCount',
		'flagsCount'
	],
	#category : #Tpminesweeper
}

{ #category : #adding }
TMinesweeperCell >> bombsCounterAdd: n [
	bombsCount := bombsCount + n
	
]

{ #category : #'initialize-release' }
TMinesweeperCell >> close [
	status := #close.
	self form: (TMinesweeperBoard catalog at: 10).
	
]

{ #category : #'initialize-release' }
TMinesweeperCell >> closeOver [
	status := #close.
	self form: (TMinesweeperBoard catalog at: 15).
	
]

{ #category : #'instance creation' }
TMinesweeperCell >> flag [
	(self isClose) ifFalse: [^self].
	status := #flagged.
	owner neighboursAt: x At: y Do: [ :cell | cell flagsCounterAdd: 1 ].
	self form: (TMinesweeperBoard catalog at: 11)

]

{ #category : #adding }
TMinesweeperCell >> flagsCounterAdd: n [
	flagsCount := flagsCount + n
	
]

{ #category : #'instance creation' }
TMinesweeperCell >> handlesMouseDown: evt [
	^true
]

{ #category : #'initialize-release' }
TMinesweeperCell >> handlesMouseOver: anEvent [
	^true
	
]

{ #category : #initialization }
TMinesweeperCell >> initialize [
	super initialize.   
	bounds := 0@0 corner: 16@16.
	bombsCount := 0.
	flagsCount := 0.
	self close.
]

{ #category : #testing }
TMinesweeperCell >> isBomb [
	^false.
]

{ #category : #testing }
TMinesweeperCell >> isClose [
	^status = #close
]

{ #category : #testing }
TMinesweeperCell >> isFlagged [
	^status = #flagged.
]

{ #category : #testing }
TMinesweeperCell >> isOpen [
	^status = #open
]

{ #category : #'instance creation' }
TMinesweeperCell >> mouseDown: evt [
	super mouseDown: evt
]

{ #category : #'initialize-release' }
TMinesweeperCell >> mouseEnter: evt [
	(self isClose) ifTrue: [ self form: (TMinesweeperBoard catalog at:15) ].
]

{ #category : #'initialize-release' }
TMinesweeperCell >> mouseLeave: evt [
	(self isClose) ifTrue: [ self form: (TMinesweeperBoard catalog at:10) ]
	
]

{ #category : #'initialize-release' }
TMinesweeperCell >> mouseUp: evt [
	super mouseUp: evt.
	(self containsPoint: evt cursorPoint)
		ifTrue: [
			"MOUSE LEFT" 
			(evt redButtonChanged) ifTrue:[ 
				(self isOpen) 
					ifTrue: [ ^self onOpen]
					ifFalse: [^self toggleFlag ]].
			"MOUSE RIGHT"
			(evt yellowButtonChanged) ifTrue: [ self open ]]
		ifFalse: [ (self isClose) ifTrue: [self close] ].
]

{ #category : #adding }
TMinesweeperCell >> onOpen [
	self subclassResponsibility
	
]

{ #category : #'instance creation' }
TMinesweeperCell >> open [
	(self isClose) ifFalse: [ ^self ].
	status := #open.
	self owner onCellOpen.
	self onOpen.
]

{ #category : #'initialize-release' }
TMinesweeperCell >> reset [
	self close.
	bombsCount := 0.
	flagsCount := 0.
	
]

{ #category : #initialization }
TMinesweeperCell >> setPositionAt: xP at: yP [
	x := xP.
	y := yP.
]

{ #category : #accessing }
TMinesweeperCell >> status [
	^status.
]

{ #category : #accessing }
TMinesweeperCell >> status: anObject [
	status := anObject
]

{ #category : #'instance creation' }
TMinesweeperCell >> toggleFlag [
	(status = #close)
		ifTrue: [ ^self flag ].
	(status = #flagged)
		ifTrue: [ self unflag ]
]

{ #category : #'as yet unclassified' }
TMinesweeperCell >> unflag [
	(self isFlagged) ifFalse: [^self].
	self closeOver
	owner neighboursAt: x At: y Do: [ :cell | cell flagsCounterAdd: -1 ].
]

{ #category : #accessing }
TMinesweeperCell >> x [
	^x
]

{ #category : #accessing }
TMinesweeperCell >> y [
	^y
]
