Class {
	#name : #TMinesweeperCellEmpty,
	#superclass : #TMinesweeperCell,
	#category : #Tpminesweeper
}

{ #category : #'instance creation' }
TMinesweeperCellEmpty >> onOpen [
	self form: (TMinesweeperBoard catalog at: bombsCount + 1).
	((bombsCount = 0) or: [ bombsCount = flagsCount  ])
		ifTrue: [ owner
				neighboursAt: x
				At: y
				Do: [ :cell | cell open ] ]
]
