tpminesweeper
====================

A minesweeper clone for Pharo.

- left mouse button: flag/unflag, open neighbours.
- right mouse button: open cell.

![GPLv3](https://www.gnu.org/graphics/gplv3-127x51.png) 
